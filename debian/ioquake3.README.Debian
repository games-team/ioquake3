ioQuake3 for Debian
===================

Debian GNU/Linux contains a modified and stripped-down version of the ioQuake3
engine. It lacks the non-free LCC compiler which usually compiles byte-code for
the Quake3 Virtual Machine; the actual virtual machine is still present, so
bytecode compiled using tools outside Debian can still be used (although it
would have to be uploaded to contrib or non-free).

If you are a integrating another great game that can benefit from the ioQuake3
you should first test if the packaged engine works for the game you are
interested in. To do so, place your game data in some directory, for example:

  /home/user/greatgame/basegg/greatgame0.pk3
  /home/user/greatgame/basegg/greatgame1.pk3

Then you can test if it works with Debian GNU/Linux's ioQuake3 by firing up
the engine with various extra command line parameters.
For "standalone" games (those that don't need any of the proprietary
Quake III: Arena or Quake III: Team Arena game content), you'll typically need:

- com_standalone to put the engine into standalone mode
- fs_basegame to make the "base game" not be baseq3
- fs_basepath to set the search path
- fs_homepath to change the directory where user settings will be saved
- cl_motd to avoid asking update.quake3arena.com whether there's an update :-)
- and perhaps other cvars, depending on your game...

For instance, OpenArena 0.8.5 looks something like this:

    /usr/lib/ioquake3/ioquake3 \
        +set com_standalone 1 \
	+set fs_basegame basegg \
	+set fs_basepath /home/user/greatgame \
	+set fs_homepath "$HOME/.greatgame" \
	+set sv_master1 master.greatgame.example.com \
	+set protocol 71 \
	+set cl_motd 0

If you are sure that your game works and you want to package it, put the
game files in /usr/share/games/greatgame (if they are
architecture-independent) or /usr/lib/greatgame (if they use native-code
cgame, qagame or ui modules), then create a wrapper script
in /usr/games that calls the ioquake3 binary with the proper command line
parameters, as shown above.

The openarena source package in Debian, and the quake source package
in the contrib component, are good examples of how to integrate a
particular game.
